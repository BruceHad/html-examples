const express = require('express');
const router = express.Router();
const fs = require('fs');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/tf', function (req, res, next) {
  res.render('tf', { title: 'Tailwind & Flexbox' });
});



module.exports = router;
